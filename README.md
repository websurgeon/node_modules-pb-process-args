# pb-process-args

## A node.js module that returns an object of key value pairs from process.argv

This is a very basic module to make it easier to check for supplied arguments when executing node.js files.

### Using Module: 

    // file.js
    var args = require('pb-process-args');
    console.log(args);

#### Result:

    $ node file.js -<shortKey> <value1> --<longKey> <value2> -<shortKey2> '<value 3>' ...
    {
        "<shortKey>": "<value1>",
        "<longKey>": "<value2>",
        "<shortKey2>": "<value 3>",
        ...
    }

NB:

- arguments must be ordered by key then value
- each key must have a value
- each key should be prefixed with `-` or `--` although this is not currently enforced
### Installing as clone:

    $ cd <path-to-store-module>
    $ git clone https://websurgeon@bitbucket.org/websurgeon/node_modules-pb-process-args.git pb-process-args

### Installing as Git Submodule:

    $ cd <path-to-existing-repo>

Create directory (if required) to store the module repo:

    $ mkdir <path-to-store-node-modules>

Command to add submodule must be run from top level of working tree:

    $ git submodule add https://websurgeon@bitbucket.org/websurgeon/node_modules-pb-process-args.git <path-to-store-node-modules>/pb-process-args