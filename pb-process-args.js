/** @fileOverview pb-process-args node.js module implementation.
 *
 * This code is made available under the MIT license.
 * The full license description is available in associated LICENSE file.
 *
 * @author Peter Barclay
 * @description See README.md for details
 *
 */

var args = {},
    argIndex,
    argLen,
    key,
    value;

for (argIndex = 2, argLen = process.argv.length; argIndex < argLen; argIndex += 2) {
    if (argIndex % 2 === 0) {
        key = process.argv[argIndex].replace(/^-{1,2}/, '');
        value = process.argv[argIndex + 1] || '';
        if (key) {
            args[key] = value;
        }
    }
}

module.exports = exports = args;
